import numpy as np
import matplotlib.pyplot as plt

f,ax = plt.subplots(1)

### Constants ###
P   = 400
m   = 75
rho = ?
C = 200
area = ???

### Time Parameters ###
dt  = 1
t_f = 180
t_n = [0]

### Velocity Parameters ###
v_0 = 1
v_n = [v_0]
v_drag_n = [v_0]


def update_velocities(t_n, v_n, v_drag):
    t_n.append(t_n[-1] + dt )
    v_n.append(v_n[-1] + P * dt/(m * v_n[-1]))

    drag_correction = ????
    v_drag_n.append(???A???  + ???B??? - drag_correction)

    return True if t_n[-1] > t_f else update_velocities(t_n, v_n, v_drag_n)


update_velocities(t_n, v_n, v_drag_n)


plt.title('Cyclist\'s Speed v. Time', fontsize=18)
plt.xlabel('Time (s)', fontsize=12)
plt.ylabel('Speed (kg)', fontsize=12)

plt.plot(t_n, v_n)
plt.plot(t_n, v_drag_n)

ax.legend(['No Drag', 'Drag'])

ax.set_ylim(bottom=0)
ax.set_xlim(left=0)
plt.show()
