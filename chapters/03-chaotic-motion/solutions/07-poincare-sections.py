import numpy as np
import sys
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button

fig, axs = plt.subplots(1)

sys.setrecursionlimit(10000)

### Constants ###
g   = 9.81
l   = g
t   = 0
dt  = 0.1
t_f = 500
q   = 0.5

### Plot Arrays ###
poincare_theta = []
poincare_omega = []

### Delta Theta Variables ###
theta = 0.5
omega = 0

### Driving Parameters ###
Freq_D   = .25
Force_D  = 1.0

def next_angular_values(t, theta, omega):
    delta_omega_grav =  g / l * np.sin(theta)
    delta_omega_damp = q * omega
    delta_omega_drive = Force_D * np.sin(Freq_D * t)
    delta_omega =  delta_omega_grav + delta_omega_damp + delta_omega_drive

    omega = omega - delta_omega * dt

    theta = theta + omega * dt
    if theta > np.pi:
        theta = theta - 2 * np.pi
    if theta <  - np.pi:
        theta =  theta + 2 * np.pi

    return theta, omega

def calc_next_step(t,  theta, omega):
    t = t + dt

    theta, omega = next_angular_values(t, theta, omega)

    if np.abs(Freq_D * t - 2 * np.pi) < 1:
        poincare_theta.append(theta)
        poincare_omega.append(omega)

    return True if t > t_f else calc_next_step(t, theta, omega)

calc_next_step(t, theta, omega)

fig.suptitle('Poincare Section', fontsize=18)
plt.scatter(poincare_theta, poincare_omega)

plt.xlabel('Theta (Rad)', fontsize=12)
plt.ylabel('Omega (Rad/S)', fontsize=12)

plt.show()
