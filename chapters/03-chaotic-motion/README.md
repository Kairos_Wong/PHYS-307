# Simple Harmonic Motion

Applying Newton\'s Second Law to a pendulum, it can be shown that:

$$F_{\theta} = -g \sin \theta$$

With a pendulum of length $l$, the angle as a function of time
$\theta(t)$ can be found from

$$\frac{d^2 \theta}{dt^2} = - \Omega_0^2 \sin \theta$$

with $\Omega_0 = \sqrt{ g/l} $

In a typical introductory course, we would Taylor expand $\sin \theta$,
which provides a solution of the form:

$$\theta(t) = \sin (\Omega_0 t + \phi)$$

Where $ \phi $ is a phase factor determined by the initial conditions in
angle and angular velocity. To begin the exploration of chaotic motion,
we will start with this simple equation and apply the Euler Method. To
do so, write the original second order differential equation in terms of
two first order differential equations:

$$\frac{d\omega}{dt} = - \Omega_0^2 \theta \qquad \frac{d\theta}{dt} = \omega$$

Now, apply the Euler Method as before:

$$\omega_{i+1} = \omega_i - \Omega \Delta t \qquad \theta_{i+1} = \theta_i + \omega_{i} \Delta t$$

## Example 1

Copy `example_1.py` into the `problems` directory. This applies the
above equation. Does it work according to your expectations? Why or why
not?

## Exploring the Euler Solution to SHM

The plot we obtained seems to violate conservation of energy. As time
goes on, with no forces other than tension and gravity, the pendulum\'s
amplitudes increase. Let\'s compare conservation of energy analytically
to this.

$$E = \frac{1}{2} m l^2 \omega ^2 + mgl(1 - \cos \theta )$$

For small $\theta$ this becomes:

$$E = \frac{1}{2} m l^2 ( \omega ^2 + \Omega_0^2 \theta ^2 )$$

Substituting the above $\omega(t)$ and $\theta(t)$ to apply the Euler
Method:

$$E_{i+1} = E_i +\frac{1}{2} m l^2 ( \omega_{i} ^ {2} + \Omega_0^2 \theta ^ 2)(\Delta t) ^ 2$$

This clearly violates conservation of energy, explaining why the plot
diverges.

## Problem 1

Modify Example 1 to preserve conservation of energy. Commit these
changes and push them to GitLab.

# Complex Harmonic Motion

There are three additional considerations to make the pendulum more
realistic: dampening, driving, and a larger angle. We will explore each
one by one before we consider the chaotic case of all three
simultaneously.

## Damped Harmonic Motion

In the case of damped harmonic motion, we add an additional term to our
simple harmonic motion:
$$\frac{d^2 \theta}{dt^2} = - \Omega_0^2  \theta - 2\beta \frac{d\theta}{dt}$$

where $q$ describes the dampening. This equation can be solved
analytically: so that:

$$\theta(t) = \theta_0 e^{-\beta t} \sin (\sqrt{\Omega_0^2 -\beta^2} t + \phi)$$

## Problem 2

This problem is a several step process to show the three kinds of motion
for a damped harmonic oscillator: Underdamped, Critically damped and
overdamped. Follow the given instructions to generate a plot of all
three scenarios.

1.  Calculate `Omega_0` in terms of the given constants
2.  Choose `beta_1`, `beta_2`, and `beta_3` to match the three damping
    cases
3.  Modify the append lines in the `update_angle` function to reflect
    the desired motion
4.  Add a legend to the plot, save it in the problems\'s directory
5.  Commit and push to Gitlab

## Driven Harmonic Motion

Driven harmonic motion is a further extension of damped harmonic motion.
Rather than a homogeneous equation, driven harmonic motion contains a
periodic driving term resulting in an equation such as:

$$\ddot{\theta}+ 2 \beta \dot{\theta} + \Omega_0^2 \theta  = F_D \sin (\Omega_D t) $$

Because this is a ordinary linear differential equation, the general
solution is composed of the superposition of the homogeneous solution,
and the particular or asymptotic solution, such that:

$$\theta_G(t) = \theta_H(t) + \theta_P(t)$$

The Homogeneous Solution $\theta_H$ was determined to be:

$$\theta_H(t) = \theta_0 e^{-\beta t} \sin (\sqrt{\Omega_0^2 -\beta^2} t + \phi)$$

previously. To determine the particular solution, convert
$\theta \rightarrow x $ and consider:

$$\ddot{x}+ 2 \beta \dot{x} + \omega_0^2 x  = F_D \sin (\omega_D t)$$

Shifting the driving frequency, so that the function becomes
$F_D \cos (\omega_D t)$ doesn\'t change the solution as this is only
shifting by a constant. Now, consider the second equation with a cosine
with the variable $y$ rather than $x$. The sine\'s and cosines can be
written in terms of exponentials if we consider:

$$z(t) = x(t) + i y(t) \qquad z(t) = C e^{i\omega} = A\cos (\omega t) + i A \sin (\omega t)$$

Adding the two equations they become:

$$(\ddot{z} + 2 \beta \dot{z} + \omega_0 z ) = F_0 e^{i\omega t}$$
$$(-\omega^2 + 2i \beta \omega + \omega_0^2) C = F_0 e^{i\omega t}$$

Solving for $C$

$$C = \frac{F_0}{(\omega_0^2 - \omega ^2)^2 + 2i \beta \omega} $$

The amplitude $\theta_0$ can be found by taking the complex conjugate
$CC^*$. Finally giving:

$$\theta(t) = \theta_0 \sin ( \Omega_1 t + \phi)$$

with $\Omega_1 = \sqrt{\Omega_0^2 -\beta^2}$. And

$$\theta_0 = \frac{F_D}{\sqrt{(\Omega_0^2 - \Omega^2_D)^2 + 4\beta^2 \Omega_D^2}}$$

## Problem 3

This problem explores driven harmonic motion.

1.  Calculate `Omega_0` in terms of the given constants
2.  Choose `beta`
3.  Choose three driving frequencies: `Omega_D_1`, `Omega_D_2`, and
    `Omega_D_2` so that at least one exhibits resonance.
4.  Modify the append lines in the `update_angle` function to reflect
    the desired motion
5.  Add a legend to the plot, save it in the problems\'s directory
6.  Commit and push to Gitlab

## Large Angle Oscillations

So far, we have taken the small angle approximation
$\sin \theta \approx \theta$, but this isn\'t true in general. The
general harmonic oscillator, takes the form

$$\frac{d^2\theta}{dt^2} = - \frac{g}{l} \sin \theta$$

Finding $\theta(t)$ follows exactly the way it does in `01-simple-hm.py`

## Problem 4

Modify problem `04-large-angle-hm.py` in order to reflect the motion for
an oscillatorn with a large amplitude. Show that the period of
oscillations depends on the initial angle by plotting several starting
angles

# Chaos in Non-Linear Systems

Chaotic Motion is observed where the above equations include all terms
becoming:

$$\ddot{\theta} + 2 \beta \dot{\theta} + \Omega_0^2 \sin \theta = F_D \sin \Omega_D t$$

Considering term by term, it can be qualitatively understood why this
produces chaotifc motion.

-   The $\beta$ term slows the pendulum. Giving it an agular velocity
    that can be \"grabbed\"
-   The $\Omega_0$ term provides a natural frequency which varies with
    the angle.
-   The driving term modifies the natural frequency as well as \"grabs\"
    the pendulum at the appropriate angular velocity

All of the above situations can be recovered by modifying the above
terms (e.g. damped harmonic motion with $\sin \theta \approx \theta$ and
$F_D$ = 0) Probing whether or not a given set of driving forces produces
chaotic motion can be done via considering:

1.  The phase plots i.e. the plots of $\theta$ vs $\omega$
2.  The plots of $\Delta\theta$, the difference between the angle of
    sets of angles for two pendulums with similar starting angles
3.  The Poincare , i.e. the phase plot for $\Omega_D t = 2 n \pi$

The case of the Poincare section is similar to what one would see with a
strobe light on a record player. At the right frequency, one would see
the label in roughly the same position. This is what is known as an
*attractor*. For small deviations of the input parameters, the plots of
the Poincare sections are \"attracted\" to the same plot. For the
following problems, use the boiler plate code provided in the problems
section or write it from (with) Scratch.

## Problem 5

Calculate $\theta$ for $F_D=0.1, 0.5, .99$ with $q=1/3$,
$\theta(0)=2/5$, and $dt=.1$. Compare the waveforms, with special
attention to the deviations from a purely sinusoidal form at high drive.

## Problem 6

Plot the difference in angle for 2 identical pendulums barring their
initial angles. Comment on the plots as you increase initial the angle
of separation between the pendulum

## Problem 7

Study the shape of the chaotic attractor for different initial
conditions. Keep the driving force fixed at $F_D=1.2$ and calculate the
attractors found for several different values of $\theta$. Show that you
obtain the same attractor even for different initial conditions,
provided that these conditions are not changed by too much. Repeat your
calculations for different values of the time step to be sure that it is
suffifciently small that it does not cause any structure in the
attractor.
