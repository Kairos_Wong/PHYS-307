import numpy as np
import sys
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button

fig, axs = plt.subplots(1)

sys.setrecursionlimit(10000)

### Constants ###
g   = 9.81
l   = g
dt  = 0.1
t_f = 200

### Initial conditions ###
t_n = [0]
q_1 = 0.5
q_2 = q_1

### Pendulum Parameters ###
l_1   = g
l_2   = l_1

omega_1 = 0
omega_2 = omega_1

omega_1_n = [omega_0]
omega_2_n = [omega_0 + 0.01]

delta_theta_n = [0]

### Driving Parameters ###
Freq_D   = .5
Force_D  = .99
Force_D_1  = 1.2
Force_D_2  = Force_D_1

### Delta Theta Variables ###
delta_theta_0 = .01
delta_theta_n = [ delta_theta_0 ]
theta_1 = 0.5
theta_2 = theta_1 + delta_theta_0

def next_angular_values(t, theta, omega, l, Force_D, Freq_D, q):
    delta_omega_grav =  g / l * np.sin(theta)
    delta_omega_damp = q * omega
    delta_omega_drive = Force_D * np.sin(Freq_D * t)
    delta_omega =  delta_omega_grav + delta_omega_damp + delta_omega_drive

    omega = omega - delta_omega * dt

    theta = theta + omega * dt
    if theta > np.pi:
        theta = theta - 2 * np.pi
    if theta <  - np.pi:
        theta =  theta + 2 * np.pi

    return theta, omega

def calc_next_step(t_n, delta_theta_n, theta_1, theta_2, omega_1, omega_2):
    t_n.append(t_n[-1] + dt )

    theta_1, omega_1 = next_angular_values(t_n[-1], theta_1, omega_1, l_1, Force_D_1, Freq_D_1, q_1)
    theta_2, omega_2 = next_angular_values(t_n[-1], theta_2, omega_2, l_2, Force_D_2, Freq_D_2, q_2)

    print(theta_2, omega_2)

    omega_1_n.append(omega_1_n[-1] - delta_omega_1 * dt)
    omega_2_n.append(omega_2_n[-1] - delta_omega_2 * dt)

    theta_1_new = theta_1_n[-1] + omega_1_n[-1] * dt
    theta_1_n.append(theta_1_new)

    theta_2_new = theta_2_n[-1] + omega_2_n[-1] * dt
    theta_2_n.append(theta_2_new)

    delta_theta_n.append(np.abs(theta_2_new - theta_1_new))

    return True if t_n[-1] > t_f else update_angle(t_n, theta_1_n, omega_1_n,  theta_2_n, omega_2_n, delta_theta_n )


update_angle(t_n, theta_1_n, omega_1_n,  theta_2_n, omega_2_n, delta_theta_n )

fig.suptitle('Chaotic Harmonic Motion', fontsize=18)
axs[0].plot(t_n, delta_theta_n)
axs[1].plot(t_n, theta_1_n)
axs[1].plot(t_n, theta_2_n)

plt.yscale('log')
plt.xlabel('Time (s)', fontsize=12)
plt.ylabel('Angle (Rad)', fontsize=12)

plt.show()
