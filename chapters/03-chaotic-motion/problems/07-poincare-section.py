import numpy as np
import sys
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button

fig, axs = plt.subplots(1)

sys.setrecursionlimit(10000)

### Constants ###
g   = 9.81
l   = g
dt  = 5
t_f = 30000

### Initial conditions ###
t_n     = [0]
q       = 1/3
beta    = 2 * q
theta_0 = 0.01
theta_n = [theta_0]

omega_0 = np.sqrt( g / l )
omega_n = [omega_0]

poincare_theta_n = [0]
poincare_omega_n = [0]

### Driving Parameters ###
Freq_D   = .5
Force_D  = .98


def update_angle(t_n, theta_n, omega_n, poincare_angle_n, poincare_omega_n):
    t_n.append(t_n[-1] + dt )
    delta_omega_grav =  g / l * np.sin(theta_n[-1])
    delta_omega_damp = q * omega_n[-1]
    delta_omega_drive = Force_D * np.sin(Freq_D * t_n[-1])
    delta_omega =  delta_omega_grav + delta_omega_damp + delta_omega_drive

    omega_n.append(omega_n[-1] - delta_omega * dt)

    theta_new = theta_n[-1] + omega_n[-1] * dt
    if theta_new > np.pi:
        theta_new = theta_new - 2 * np.pi
    if theta_new <  - np.pi:
        theta_new =  theta_new + 2 * np.pi

    theta_n.append(theta_new)

    ### Caculator poincare section here ###

    return True if t_n[-1] > t_f else update_angle(t_n,theta_n, omega_n, poincare_theta_n, poincare_omega_n)

def update_angle_iterator(t_n, theta_n, omega_n, poincare_angle_n, poincare_omega_n):
    for i in range(t_f // t_n):
        t_n.append(t_n[-1] + dt)
        delta_omega_grav = g / l * np.sin(theta_n[-1])
        delta_omega_damp = q * omega_n[-1]
        delta_omega_drive = Force_D * np.sin(Freq_D * t_n[-1])
        delta_omega =  delta_omega_grav + delta_omega_damp + delta_omega_drive

        omega_n.append(omega_n[-1] - delta_omega * dt)

        theta_new = theta_n[-1] + omega_n[-1] * dt
        if theta_new > np.pi:
            theta_new = theta_new - 2 * np.pi
        if theta_new <  - np.pi:
            theta_new =  theta_new + 2 * np.pi

        theta_n.append(theta_new)
        return True if t_n[-1] > t_f else update_angle_iterator(t_n,theta_n, omega_n, poincare_theta_n, poincare_omega_n)


update_angle_iterator(t_n, theta_n, omega_n, poincare_theta_n, poincare_omega_n)

fig.suptitle('Chaotic Harmonic Motion', fontsize=18)

plt.scatter(theta_n, omega_n, s=0.2)

t_n     = [0]
q       = 1/3
beta    = 2 * q
theta_0 = 0.2
theta_n = [theta_0]

omega_0 = np.sqrt( g / l )
omega_n = [omega_0]

update_angle_iterator(t_n, theta_n, omega_n, poincare_theta_n, poincare_omega_n)
plt.scatter(theta_n, omega_n, s=0.2)

plt.ylabel('Angle (Rad)', fontsize=12)
plt.xlabel('Omega (Rad/s)', fontsize=12)

plt.show()
