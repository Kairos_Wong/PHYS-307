import numpy as np
import matplotlib.pyplot as plt

f,ax = plt.subplots(1)

### Constants ###
g   = 9.81
l   = 2.5
dt  = 0.1
t_f = 10

### Initial Conditions ###
Omega_0 = 1
beta_1  = 1# Underdamped
beta_2  = 1# Critically Damped
beta_3  = 1# Overdamped

### Arrays ###
t_n     = [0]
omega_0 = 0.5
omega_n = [omega_0]
theta_0 = np.pi / 10
theta_n = [theta_0]

beta_array = [ beta_1, beta_2, beta_3 ]

def update_angle(t_n, beta, theta_n, omega_n):

    t_n.append(t_n[-1] + dt )
    omega = np.sqrt(g*l)
    if beta >= omega:
        omega_n.append(omega_n[-1] - g / l * theta_n[-1] * dt) 
        theta_n.append(theta_n[-1] + theta_0 * np.exp(-beta * t_n[-1]) * np.cosh(np.sqrt(np.absolute(beta ** 2 - omega ** 2))*d_t[-1]))
    else:
        omega_n.append(omega_n[-1] - g / l * theta_n[-1] * dt) 
        theta_n.append(theta_n[-1] + theta_0 * np.exp(-beta * t_n[-1]) * np.cos(np.sqrt(np.absolute(pow(beta,2) - pow(omega,2)))*t_n[-1]))

    return True if t_n[-1] > t_f else update_angle(t_n, beta, theta_n, omega_n)

def gen_beta_plots():
    for beta in beta_array:
        t_n = [0]; theta_n = [theta_0]; omega_n=[omega_0]
        update_angle(t_n, beta, theta_n, omega_n)
        ax.plot(t_n, theta_n)

gen_beta_plots()

plt.title('Damped Harmonic Motion', fontsize=14)
plt.xlabel('Time (s)', fontsize=12)
plt.ylabel('Angle (Rad)', fontsize=12)

ax.set_xlim([0,10])

plt.show()
