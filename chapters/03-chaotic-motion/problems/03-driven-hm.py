import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button

### Constants ###
g   = 9.81
l   = 2.5
dt  = 0.1
t_f = 10

### Initial conditions ###
t_n     = [0]
F_D     =  5
beta    = ### ??? ###
theta_0 = np.pi / 10
theta_n = [theta_0]
omega_0 = 0
omega_n = [omega_0]

### Driving Parameters ###
Omega_0   = 0
Omega_D_1 = 2 * beta
Omega_D_2 = Omega_0 ** 2 * np.sin(theta_n[-1])
Omega_D_3 = 
Omega_D_array = [ Omega_D_1, Omega_D_2, Omega_D_3 ]

def update_angle(t_n, beta, theta_n, omega_n, Omega_D):

    t_n.append(t_n[-1] + dt )
    omega_n.append(omega_n[-1] - )
    theta_n.append(theta_n[-1] + )

    if beta >= omega:
        omega_n.append(omega_n[-1] - ??????)
        theta_n.append(theta_n[-1] + ??????)
    else:
        omega_n.append(omega_n[-1] - ??????)
        theta_n.append(theta_n[-1] + ??????)

    return True if t_n[-1] > t_f else update_angle(t_n, beta, theta_n, omega_n, Omega_D)

def gen_Omega_D_plots():
    for Omega_D in Omega_D_array:
        t_n = [0]; theta_n = [angle]; omega_n=[omega_0]
        update_angle(t_n, beta, theta_n, omega_n, Omega_D)
        ax.plot(t_n, theta_n)

gen_beta_plots()

plt.title('Damped Harmonic Motion', fontsize=18)
plt.xlabel('Time (s)', fontsize=12)
plt.ylabel('Angle (Rad)', fontsize=12)

#ax.set_xlim(left=0)
ax.set_xlim([0,10])

plt.show()
