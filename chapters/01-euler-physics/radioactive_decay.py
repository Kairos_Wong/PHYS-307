import numpy as np
import matplotlib.pyplot as plt

f,ax = plt.subplots(1)

### Time Parameters ###
t_half   = 5700 # Half-Life
tau      = ? # Decay Constant
dt       = 0 # Time Step Width
T_n      = [0] # Time Step Array

### Carbon 14 Parameters ###
C14     =   # Initial Population
C14_rem =     # C14 Remaining Cutoff
C14_n   = [C14]    # C14 Population Array

### Recursive Function to Update Population ###
def decay(T_n, C14_n):

    # Some calculation

    if C14_n[last] < C14_rem:
        return
    else:
        decay(T_n, C14_n)

### Will recurse until exit condition is met
decay(T_n, C14_n)

### Print Plots ###
plt.title('', fontsize=18)
plt.xlabel('', fontsize=12)
plt.ylabel('', fontsize=12)

ax.set_ylim(bottom=0)
ax.set_xlim(left=0)

## T_n and C14_n must be arrays
plt.plot(T_n,C14_n)
plt.show()
