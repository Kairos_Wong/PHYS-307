import numpy as np
import sys
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button

fig, axs = plt.subplots(2)

xarr = [0]
freqarr = [0 for i in range(1000)]
end = [0]

min = 1000
max = 0

def flip_coin():
    add = 1 if np.random.randint(0, 2) == 0 else -1
    xarr.append(xarr[-1] + add)

for j in range(1000):
    xarr = [0]
    for i in range(1000):
        flip_coin()

    if(xarr[-1] < min):
        min = xarr[-1]
    if(xarr[-1] > max):
        max = xarr[-1]

    freqarr[xarr[-1] + 500] += 1
    end.append(xarr[-1])

t = [i for i in range(len(xarr))]
pos = [i-500 for i in range(len(freqarr))]

print(min)
print(max)
axs[1].scatter(t ,xarr, s=0.5)
#axs[0].hist(xarr,40)
axs[0].hist(end, 40)

plt.show()

sys.setrecursionlimit(100000)
