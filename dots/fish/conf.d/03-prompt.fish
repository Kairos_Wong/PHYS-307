#!/usr/bin/env fish

## Continue if non-interactive Shell ##
   status is-interactive || exit

   ## Remove Greeting and Mode Prompt ##
      function fish_mode_prompt;  end
      set -U fish_greeting

   ## Set vi mode ##
   	  set -U fish_key_bindings fish_vi_key_bindings

   ## Set Cursors and Launch Starship ##
      set fish_cursor_replace_one underscore
      set fish_cursor_default     block
      set fish_cursor_visual      block
      set fish_cursor_insert      line
      starship init fish | source

