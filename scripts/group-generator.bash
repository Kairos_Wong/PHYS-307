#!/usr/bin/env bash

NAMES=( 'Albert' 'Anas' 'Jasper' 'Kairos' 'Kenny' 'Prannoy' 'William' 'Wu' )
NAMES=( $(printf "%s\n" "${NAMES[@]}" | shuf | tr '\n' ' ') )

i=1
while [[ ${#NAMES[@]} -gt 0 ]]; do
    echo "Team $i: ${NAMES[0]} ${NAMES[1]}"
      NAMES=("${NAMES[@]:1}"); NAMES=("${NAMES[@]:1}")
      i=$((i+1))
done
